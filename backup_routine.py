from anonymization.anonymize import get_anonymized_list, set_client_info_to_anonymized
from connections.mongodb_conn import MongodbConnection

'''
    * the prints shuld be replaced for a log function if cast to production
    * exit(1) should be configureted to contact Admin on backup job, since some erro happened
'''
mongo_db_clients = MongodbConnection().connect_to_clients()
has_error = False
for client_id in get_anonymized_list():
    try:      
        print(f'anonymizing data for client {client_id}')
        set_client_info_to_anonymized(client_id, mongo_db_clients)
    except Exception as e:
        print(f'Error while anonymizing cpf {client_id}: {e})')
        has_error = True
mongo_db_clients.close

if has_error:
    exit(1)
    

from connections.mongodb_conn import MongodbConnection
from datetime import datetime

ANONYMIZED = "anonymized"

def check_cpf(cpf, mongo_db_clients):
    client_id = mongo_db_clients.find_one({"cpf": cpf}, {"_id":1})
    if not client_id:
        return False
    return client_id["_id"]
    
def set_client_info_to_anonymized(client_id, mongo_db_clients):
    r= mongo_db_clients.update_one(
        {"_id": client_id}, 
        {"$set": {
            "cpf": ANONYMIZED,
            "cep":ANONYMIZED,
            "house_number":ANONYMIZED            
            }})
    print(r.matched_count)
    
def add_to_anonymized_list(client_id, mongo_db_anonymizeds):
    mongo_db_anonymizeds.insert_one(
            {
                "client_id": client_id,
                "anonymized_at": datetime.now(),
            }
        )

    
def anonymize(cpf):
    try:
        mongo_db_clients = MongodbConnection().connect_to_clients()
        client_id = check_cpf(cpf, mongo_db_clients)
        if not client_id:
            mongo_db_clients.close
            return "cpf not found"
        
        mongo_db_anonymizeds = MongodbConnection().connect_to_anonymizeds()
        add_to_anonymized_list(client_id, mongo_db_anonymizeds)
        mongo_db_anonymizeds.close
        
        set_client_info_to_anonymized(client_id, mongo_db_clients)
        mongo_db_clients.close

        return "anonymized-ok"
    except Exception as e:
        return e

def get_anonymized_list():
    mongo_db_anonymizeds = MongodbConnection().connect_to_anonymizeds()
    anonymizeds = mongo_db_anonymizeds.find(
            {}, {"client_id":1,"_id":0}
        )
    anonymizeds_list = []
    for element in anonymizeds:
        anonymizeds_list.append(element['client_id'])
    mongo_db_anonymizeds.close
    return anonymizeds_list
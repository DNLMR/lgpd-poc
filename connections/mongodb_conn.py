from pymongo import MongoClient


class MongodbConnection:
   
   
    @staticmethod
    def connect_to_mongodb():
        return MongoClient(
            'mongodb://127.0.0.1:27017'
        )
        
    @staticmethod
    def connect_to_mongodb_main_node():
        return MongoClient(
            'mongodb://127.0.0.1:27017'
        )['lgpd-poc']

    @staticmethod
    def connect_to_mongodb_anonymized_node():
        return MongoClient(
            'mongodb://127.0.0.1:27017'
        )['anonymized'] 
        
    def connect_to_clients(self):  
        conn = self.connect_to_mongodb_main_node()  
        return conn['clients']
    
    def connect_to_orders(self):  
        conn = self.connect_to_mongodb_main_node()  
        return conn['orders']
    
    def connect_to_anonymizeds(self):  
        conn = self.connect_to_mongodb_anonymized_node()  
        return conn['anonymizeds']

# LGPD-POC
 
# ABOUT THE PROJECT
 
This repo wants to show a 'LGPD' demonstration applied in a small and mocked system.
User Story   | Solution
--------- | ------
A client for a ecommerce platform wants his sensitivity data to be 'erased' | Backend API that anonymize this particular client traceability via anonymization
A backup routine is triggered and must keep anonymized clients up to date | Python scripts to anonymized client list in order to keep it up to date (this solution is considering the 'core' base with client data is on a separate machine than the anonymize db)
 
# RUNNING THE PROJECT
 
1. Start mongodb server with the following command. If you don't have it installed follow [this guide.](https://www.mongodb.com/docs/manual/installation/)
```
sudo systemctl start mongod
```
2. Clone this repo and on its main folder, run the follow command to install Python dependencies:
```
pip install -r requirements.txt
```
3. Initiate FastAPI 'server' by running the following command:
```
uvicorn main:app --reload
```
4. In order to generate random and mocked data for test this solution make a `POST` at this endpoint:
```
http://127.0.0.1:8000/generate_random_dataset
```
5. Access the Anonymize method through the following endpoint using the `POST` method. Don't forget to change path param to a valid cpf - random generated on step 4.
```
http://127.0.0.1:8000/anonymize/{cpf}
```  
6. OPTIONAL Step - In order to delete/clean the test dataset, make a call using `DELETE` method at this endpoint:
```
http://127.0.0.1:8000/clean_dataset
``` 
7. OPTIONAL Step - Run the command below to check how 'post backup' would behave:
```
python3 backup_routine.py
```
 
# GUIDELINES
https://stefanini.com/pt-br/trends/artigos/o-que-e-anonimizacao  
https://leadcomm.com.br/2020/07/29/a-lgpd-e-a-anonimizacao-de-dados-mascaramento-criptografia-e-tokenizacao/  
https://fastapi.tiangolo.com/

from connections.mongodb_conn import MongodbConnection

def atomic_bomb():
    mongo_db = MongodbConnection().connect_to_mongodb()
    mongo_db.drop_database('lgpd-poc')
    mongo_db.drop_database('anonymized')
    mongo_db.close

    print('ggwp-nothing-left')
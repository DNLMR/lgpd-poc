from connections.mongodb_conn import MongodbConnection
import random

def generate_cpf():
    cpf = [random.randrange(10) for _ in range(9)]

    for _ in range(2):
        value = sum([(len(cpf) + 1 - i) * v for i, v in enumerate(cpf)]) % 11
        cpf.append(11 - value if value > 1 else 0)

    return "".join(str(x) for x in cpf)


def generate_clients_docs(dataset_size):
    mongo_db_clients = MongodbConnection().connect_to_clients()

    for _ in range(dataset_size):
        while True:
            cpf =  generate_cpf()
            x = mongo_db_clients.find_one({"cpf": cpf})
            if not x:
                break
            
        mongo_db_clients.insert_one(
            {
                "cpf": cpf,
                "cep": f"{random.randint(0,99999):06d}-000",
                "house_number": random.randint(1,100)
            }
        )

    mongo_db_clients.close

    print('ggwp-clients')
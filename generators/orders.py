from pydoc import cli
import random
from datetime import timedelta, datetime

from connections.mongodb_conn import MongodbConnection

D_START = datetime.strptime('1/1/2011 10:00 PM', '%m/%d/%Y %I:%M %p')
D_END = datetime.strptime('12/31/2021 10:00 AM', '%m/%d/%Y %I:%M %p')

def random_date(start, end):
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)

def generate_order(account_id, order_number, products_list):

    itens = random.sample(products_list, k=random.randint(1,6))
    itens_array = [{"sku": item} for item in itens]
    order_number+=1
    order = {
        "account_id": account_id,
        "order_number":f"{order_number:06d}",
        "itens": itens_array,
        "created_at": random_date(D_START, D_END)
    }
    return order

def generate_orders_for_client(account_id, total_orders,mongo_db_orders, products_list):
    
    for x in range(total_orders):
        order = generate_order(account_id=account_id, order_number=x, products_list=products_list)
        mongo_db_orders.insert_one(order)
    

def get_all_clients():
    mongo_db_clients = MongodbConnection().connect_to_clients()
    clients = mongo_db_clients.find({}, {"_id":1})
    mongo_db_clients.close
    return [client for client in clients]

def generate_orders_docs():
    clients = get_all_clients()
    products_list = [1001,1002,1003,1004,1005,1006]

    mongo_db_orders = MongodbConnection().connect_to_orders()

    for client in clients:
        generate_orders_for_client(
            account_id=client.get('_id'),
            total_orders=random.randint(1,3),
            mongo_db_orders=mongo_db_orders,
            products_list=products_list
        )
    
    mongo_db_orders.close
    print('ggwp-orders')

    



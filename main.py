from anonymization.anonymize import anonymize
from generators.clients import generate_clients_docs
from generators.orders import generate_orders_docs
from destroyer.destroy import atomic_bomb

from typing import Optional
from fastapi import FastAPI, HTTPException, Response, status

app = FastAPI()

@app.get("/")
def root():
    return {"Hello": "World"}

@app.post("/generate_random_dataset")
def generate_random_dataset(total_cpf: Optional[int] = 10):
    try:
        generate_clients_docs(dataset_size=total_cpf)
        generate_orders_docs()    
        return {"detail":"ok"}
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=e)

@app.post("/anonymize/{cpf}")
def anonymize_cpf(cpf: str):
    result = anonymize(cpf=cpf)
    if result == 'cpf not found':
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item not found")
    if result != 'anonymized-ok':
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(result))
    
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@app.delete("/clean_dataset")
def clean_dataset():
    try:
        atomic_bomb()
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
        